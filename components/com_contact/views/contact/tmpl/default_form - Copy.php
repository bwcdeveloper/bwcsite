<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

JFactory::getDocument()->addScript("https://maps.googleapis.com/maps/api/js");
JFactory::getDocument()->addScriptDeclaration("
		function initMap() {
			var address = 'Via Alessandro Volta, 22\\n20094 Corsico (MI)';
			var myLatLng = {lat: 45.444718, lng: 9.105157}, zoomLevel = 14;

			var mapObj = new google.maps.Map(
				document.getElementById('googleMap'),
				{
					center: myLatLng,
					zoom: zoomLevel,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}
			);
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: mapObj,
				title: 'Businessware Consulting s.r.l.\\n' + address
			});
		}
		google.maps.event.addDomListener(window, 'load', initMap);
");
JFactory::getDocument()->addStyleDeclaration("
div#googleMap {
	height: 400px;
	width: 90%;
	background-color: White;
	margin: 0 0 30px;
}
table.contacts {
	width: 90%;
}
");

if (isset($this->error)) : ?>
	<div class="contact-error">
		<?php echo $this->error; ?>
	</div>
<?php endif; ?>

<div class="contact-form">
	<h2>Businessware Consulting s.r.l.</h2>
	<table class="contacts">
		<tr><td>P.IVA / C.F.</td><td>05386770969</td></tr>
		<tr><td>Telefono</td><td>+39 02 36597407</td></tr>
		<tr><td>E-mail</td><td>info@bwcteam.com</td></tr>
		<tr><td>Indirizzo</td><td>Via Alessandro Volta, 22 - 20094 Corsico (MI)</td></tr>
	</table>
	<div id="googleMap"></div>		
	<form id="contact-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal">
		<h2>Inviaci un messaggio compilando il form sottostante</h2>
		<!--fieldset>
			<legend>< ?php echo JText::_('COM_CONTACT_FORM_LABEL'); ? ></legend-->
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_name'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_email'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_email'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_subject'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_subject'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_message'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_message'); ?></div>
			</div>
			<?php if ($this->params->get('show_email_copy')) : ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_email_copy'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_email_copy'); ?></div>
				</div>
			<?php endif; ?>
			<div class="control-group">
				<div class="control-label">
				<label id="jform_privacy_policy-lbl" for="jform_privacy_policy" class="hasTooltip" title="" data-original-title="Esplicitare il consenso al trattamento dei dati personali nei limiti consentiti dalla legge.">
					Ho preso visione dell'<a href="/joomla/includes/InformativaPrivacy.html" target="_blank">informativa</a> e acconsento * </label>
				</div>
				<div class="controls">
					<input type="checkbox" class="required" name="jform[privacy_policy]" id="jform_privacy_policy" value="1" required="required" aria-required="true">
				</div>
			</div>
			<?php // Dynamically load any additional fields from plugins. ?>
			<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
				<?php if ($fieldset->name != 'contact') : ?>
					<?php $fields = $this->form->getFieldset($fieldset->name); ?>
					<?php foreach ($fields as $field) : ?>
						<div class="control-group">
							<?php if ($field->hidden) : ?>
								<div class="controls">
									<?php echo $field->input; ?>
								</div>
							<?php else: ?>
								<div class="control-label">
									<?php echo $field->label; ?>
									<?php if (!$field->required && $field->type != "Spacer") : ?>
										<span class="optional"><?php echo JText::_('COM_CONTACT_OPTIONAL'); ?></span>
									<?php endif; ?>
								</div>
								<div class="controls"><?php echo $field->input; ?></div>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			<?php endforeach; ?>
			<div class="form-actions">
				<button class="btn btn-primary validate" type="submit"><?php echo JText::_('COM_CONTACT_CONTACT_SEND'); ?></button>
				<input type="hidden" name="option" value="com_contact" />
				<input type="hidden" name="task" value="contact.submit" />
				<input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
				<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
				<?php echo JHtml::_('form.token'); ?>
			</div>
		<!--/fieldset-->
	</form>
</div>
